const ErrorModel = require("../errors/errorModel");

module.exports = function (err, req, res, next) {
  if (err instanceof ErrorModel) {
    return res.status(err.status).json({ message: err.message });
  }

  return res.status(500).json({ message: "Server error" });
};
