function getRequestLog(req) {
  const current_datetime = new Date();
  const formatted_date =
    current_datetime.getFullYear() +
    "-" +
    (current_datetime.getMonth() + 1) +
    "-" +
    current_datetime.getDate() +
    " " +
    current_datetime.getHours() +
    ":" +
    current_datetime.getMinutes() +
    ":" +
    current_datetime.getSeconds();
  const reqMethod = req.method;
  const reqURL = req.protocol + "://" + req.get("host") + req.originalUrl;
  const log = `[${formatted_date}] ${reqMethod} ${reqURL}`;
  console.log(log);
}

function getResponseLog(res) {
  const oldWrite = res.write,
    oldEnd = res.end;
  const chunks = [];

  res.write = function (chunk) {
    chunks.push(chunk);

    return oldWrite.apply(res, arguments);
  };

  res.end = function (chunk) {
    if (chunk) chunks.push(chunk);

    let body = Buffer.concat(chunks).toString("utf8");
    const log = `Status ${res.statusCode} ${body}`;
    console.log(log);

    oldEnd.apply(res, arguments);
  };
}

module.exports = async function (req, res, next) {
  getRequestLog(req);
  getResponseLog(res);

  next();
};
