class FileHelper {
  static getFileExtension(filename) {
    const file = filename.split(".");
    return file[file.length - 1];
  }

  static getFilesResponse(files) {
    return {
      message: "Success",
      files: files ? files : [],
    };
  }

  static getFileResponse(filename, content, uploadedDate) {
    return {
      message: "Success",
      filename: filename,
      content: content.toLocaleString(),
      extension: FileHelper.getFileExtension(filename),
      uploadedDate,
    };
  }
}

module.exports = FileHelper;
