const fs = require("fs");
const path = require("path");
const FileHelper = require("../services/fileHelper");
const ErrorModel = require("../errors/errorModel");
const FILE_DIRECTORY_PATH = path.join(__dirname, "..", "files");

class FileController {
  async getFiles(req, res, next) {
    if (fs.existsSync(FILE_DIRECTORY_PATH)) {
      await fs.readdir(FILE_DIRECTORY_PATH, (err, files) => {
        if (err) {
          return next(ErrorModel.internal("Server error"));
        }

        res.json(FileHelper.getFilesResponse(files));
      });
    } else {
      res.json(FileHelper.getFilesResponse());
    }
  }

  async getFile(req, res, next) {
    const { filename } = req.params;

    if (!filename.trim()) {
      return next(ErrorModel.badRequest(`Empty filename`));
    }

    if (filename) {
      if (fs.existsSync(FILE_DIRECTORY_PATH)) {
        await fs.readFile(
          path.join(FILE_DIRECTORY_PATH, filename),
          (err, content) => {
            if (err) {
              return next(
                ErrorModel.badRequest(
                  `No file with '${filename}' filename found`
                )
              );
            }

            fs.stat(path.join(FILE_DIRECTORY_PATH, filename), (err, stats) => {
              if (err) {
                return next(ErrorModel.internal("Server error"));
              }

              res.json(
                FileHelper.getFileResponse(filename, content, stats.birthtime)
              );
            });
          }
        );
      } else {
        return next(
          ErrorModel.badRequest(`No file with '${filename}' filename found`)
        );
      }
    } else {
      return next(ErrorModel.internal("Server error"));
    }
  }

  async createFile(req, res, next) {
    const { filename, content } = req.body;
    const extensions = ["log", "txt", "json", "yaml", "xml", "js"];

    if (!content) {
      return next(ErrorModel.badRequest("Please specify 'content' parameter"));
    } else if (!filename) {
      return next(ErrorModel.badRequest("Please specify 'filename' parameter"));
    }

    if (
      extensions.indexOf(FileHelper.getFileExtension(filename)) === -1 ||
      filename.indexOf(".") === -1
    ) {
      return next(ErrorModel.badRequest("Wrong extension"));
    }

    if (!fs.existsSync(path.join(FILE_DIRECTORY_PATH, filename))) {
      !fs.existsSync(FILE_DIRECTORY_PATH) && fs.mkdirSync(FILE_DIRECTORY_PATH);

      await fs.writeFile(
        path.join(FILE_DIRECTORY_PATH, filename),
        content,
        (err) => {
          if (err) {
            return next(ErrorModel.internal("Server error"));
          }
          res.json({ message: "File created successfully" });
        }
      );
    } else {
      return next(ErrorModel.badRequest(`File ${filename} already exists`));
    }
  }

  async updateFile(req, res, next) {
    const { filename } = req.params;
    const { content } = req.body;

    if (!filename.trim()) {
      return next(ErrorModel.badRequest(`Empty filename`));
    }

    if (filename) {
      if (fs.existsSync(path.join(FILE_DIRECTORY_PATH, filename))) {
        if (!content) {
          return next(
            ErrorModel.badRequest("Please specify 'content' parameter")
          );
        }

        await fs.writeFile(
          path.join(FILE_DIRECTORY_PATH, filename),
          content,
          (err) => {
            if (err) {
              return next(ErrorModel.internal("Server error"));
            }
            res.json({ message: "File updated successfully" });
          }
        );
      } else {
        return next(
          ErrorModel.badRequest(`No file with '${filename}' filename found`)
        );
      }
    } else {
      return next(ErrorModel.internal("Server error"));
    }
  }

  async deleteFile(req, res, next) {
    const { filename } = req.params;

    if (!filename.trim()) {
      return next(ErrorModel.badRequest(`Empty filename`));
    }

    if (filename) {
      if (fs.existsSync(path.join(FILE_DIRECTORY_PATH, filename))) {
        fs.unlink(path.join(FILE_DIRECTORY_PATH, filename), (err) => {
          if (err) {
            return next(ErrorModel.internal("Server error"));
          }
          res.json({ message: "File deleted successfully" });
        });
      } else {
        return next(
          ErrorModel.badRequest(`No file with '${filename}' filename found`)
        );
      }
    } else {
      return next(ErrorModel.internal("Server error"));
    }
  }
}

module.exports = new FileController();
