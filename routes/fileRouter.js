const Router = require("express");
const router = Router();
const fileController = require("../controllers/fileController");
const logMiddleware = require("../middleware/logMiddleware");

router.get("/", logMiddleware, fileController.getFiles);
router.get("/:filename", logMiddleware, fileController.getFile);

router.post("/", logMiddleware, fileController.createFile);
router.put("/:filename", logMiddleware, fileController.updateFile);
router.delete("/:filename", logMiddleware, fileController.deleteFile);

module.exports = router;
