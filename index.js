const express = require("express");
const cors = require("cors");

const app = express();
const PORT = 8080;
const router = require("./routes/fileRouter");
const errorMiddleware = require("./middleware/errorMiddleware");
const logMiddleware = require("./middleware/logMiddleware");

app.use(cors());
app.use(express.json());
app.use("/api/files", router);
app.use(errorMiddleware);
app.use(logMiddleware);

app.listen(PORT, () => {});
