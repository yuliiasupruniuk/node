class ErrorModel extends Error {
  constructor(status, message) {
    super();
    this.status = status;
    this.message = message;
  }

  static badRequest(message) {
    return new ErrorModel(400, message);
  }

  static internal(message) {
    return new ErrorModel(500, message);
  }
}

module.exports = ErrorModel;
